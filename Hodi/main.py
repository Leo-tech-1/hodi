import requests 
from pprint import pprint
from flask.views import MethodView
from wtforms import Form, StringField, SubmitField
from flask import Flask, render_template, request, url_for

app = Flask(__name__)

class HomePage(MethodView):

    def get(self):
        return render_template("index.html")

    def post(self):
        return render_template("index.html",
        #happybirthday = happy_birthday,
        result = True)

class SignUpPage(MethodView):

    def get(self):
        return render_template("signup.html")


class HappyBirthday:

    def __init__(self, data, language):
        self.data = data
        self.language = language

    def get(self):
        if self.data["Language"] == self.language:
            print(self.data["Translation"])
        else:
            print("Sorry we do not have this language curently in the dataset!")

# url = ""

# response = requests.get(url)
# content = response.json()
# x = content['language']
# pprint(x)

app.add_url_rule("/", view_func=HomePage.as_view("Home_Page"))
app.add_url_rule("/signup", view_func=SignUpPage.as_view("SignUp_Page"))

app.run(debug=True)




# if __name__ == '__main__':
#     app.run()