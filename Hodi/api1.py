from flask import Flask, request, jsonify, redirect, render_template
from neo4j import  GraphDatabase
from pprint import pprint

# establishing the connection:


class APIAccess:
   
   def __init__(self, uri, user, password):
       self.driver = GraphDatabase.driver(uri, auth=(user, password))

   def close(self):
       self.driver.close()

   def print_greeting(self):
        with self.driver.session() as session:
            greeting = session.read_transaction(self._create_and_return_greeting)
            print("The total no. of nodes is", greeting)

   @staticmethod
   def _create_and_return_greeting(tx):
        nodes = tx.run("match (l:Language) return l")
        for node in nodes:
            pprint(node)
        



if __name__=="__main__":
    greeter = APIAccess("bolt://85.25.130.83:17687", "neo4j", "nodes2020")
    greeter.print_greeting()
    greeter.close()


