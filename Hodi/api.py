from flask import Flask, request, jsonify, redirect, render_template
from neo4j import  GraphDatabase
from pprint import pprint

# establishing the connection:


class APIAccess:
   
   def __init__(self, uri, user, password):
       self.driver = GraphDatabase.driver(uri, auth=(user, password))

   def close(self):
       self.driver.close()

   def print_greeting(self):
        with self.driver.session() as session:
            greeting = session.read_transaction(self._create_and_return_greeting, "tir")
            print("The total no. of nodes is", greeting)

   @staticmethod
   def _create_and_return_greeting(tx, language_code):
       language = []
       result = tx.run("MATCH(l:Language)<-[w:WRITTEN_IN]-(:Smurf)-[:PART_OF]->(:PhraseDuck{text:'Happy birthday'}"
                       "RETURN DISTINCT l.code, w.variant,l.Google_LangName"
                       "ORDER BY l.code ASC", language_code=language_code);
       for record in result:
          language.append(record["l.Google_LangName"])
       return language




if __name__=="__main__":
    greeter = APIAccess("bolt://85.25.130.83:17687", "neo4j", "nodes2020") 
    greeter.print_greeting()
    greeter.close()


